--[[
	Hard Trees Redo - Prevents digging trees by punching them.
	Copyright © 2018, 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Groups to be assigned
--

local groups_cacti = {
	groups = {choppy = 1, dl_bc = 1, not_in_creative_inventory = 1}
}

if minetest.get_modpath("fallen_trees") then
	groups_cacti = {
		groups = {
			choppy = 1, dl_bc = 1, not_in_creative_inventory = 1,
			falling_node = 1
		}
	}
end


--
-- Nodes to be overriden
--

local nodes_cacti = {
	"desert_life:barrel_cacti_1_sp", "desert_life:barrel_cacti_2_sp",
	"desert_life:barrel_cacti_3_sp"
}


--
-- Nodes overriders
--

for n = 1, 3 do
	minetest.override_item(nodes_cacti[n], groups_cacti)
end


--
-- Flush the no longer used tables
--

groups_cacti = nil
nodes_cacti = nil
