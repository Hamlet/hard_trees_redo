# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.


## [0.2.0] - 2019-11-09
### Added

	- Support for Minetest Game v5.x new nodes.
	- Support for [Bonemeal][1].

### Changed

	- License changed to [EUPL v1.2][2].
	- mod.conf set to follow Minetest v5.x specifics.
	- Textures have been optimized with [optipng][3].
	- Code cleaning.

### Removed

	- Support for Minetest Game v0.4.x
	- ../doc/


## [0.1.1] - 2018-04-23
### Added

	- ../doc/

### Changed

	- Node overriders now use the "for" cycle, where possible, this makes easier to read and mantain the code.


[1]: https://forum.minetest.net/viewtopic.php?t=16446
[2]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[3]: http://optipng.sourceforge.net/
